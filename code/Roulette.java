import java.util.InputMismatchException;
import java.util.Scanner;
public class Roulette{
    public static void main(String[] args){
        RouletteWheel wheel = new RouletteWheel();
        Scanner keyboard = new Scanner(System.in);
        int user = 1000; //currency
        while(true){ //loops until player wants to stop
            System.out.println("Would you like to place a bet? (y/n) Available: " + user + " $");
            String decision = keyboard.next();
            if(decision.equals("n") || user == 0){ //what happens if the user has no $$$ or picks "n"
                break;
            }else if(decision.equals("y")){
                int betDecision = 0;
                int numberDecision = 0;
                while(true){ //loops until user has good input on the bet
                    try{
                        System.out.println("Enter the correct amount.");
                        betDecision = keyboard.nextInt();
                        if(user <= 0 || betDecision < 0 || user < betDecision){ //checks the money input
                            betDecision = 0; //prevents adding money to user with negative numbers
                            System.out.println("Please check your available balance or enter a possible bet.");
                            break;
                        }
                        user -= betDecision;
                        while(true){ //loops until user has good input on 0-37
                            try{
                                System.out.println("Input a number between 0 and 37.");
                                numberDecision = keyboard.nextInt();
                                if(numberDecision < 0 || numberDecision > 37){
                                    numberDecision = 0;
                                    System.out.println("Please check if you inputted a valid number between 0 and 37.");
                                    continue;
                                }
                                wheel.spin(); //spinning the roulette
                                user+=checkWin(numberDecision,wheel.getValue(), betDecision); //calling a method iny the Roulette class
                                break;
                            }catch(InputMismatchException e){
                                System.out.println("Error:Enter a NUMBER 0-37!");
                                keyboard.next();
                                continue;
                            }
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Error:Enter a NUMBER to bet!");
                        keyboard.next();
                        continue;
                    }
                }
            }else{
                System.out.println("Please enter y or n.");
                continue;
            }
        }
    }
    public static int checkWin(int userNumber, int rouletteNumber, int userBet){
        int betWon = 0; //be returned
        if(userNumber == rouletteNumber){
            betWon = (userBet * 35)+userBet; //player gets back the amount that he betted with the won bonus
            System.out.println("Winner!");
        }else{
            System.out.println("You have lost.");
        }
        return betWon;
    }
}