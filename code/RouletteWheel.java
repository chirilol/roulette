import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int number = 0;
    RouletteWheel(){
        this.random = new Random();
    }
    public void spin(){
        this.number = this.random.nextInt(38);
    }
    public int getValue(){
        return this.number;
    }
}
